import React, {Component} from 'react';
import GoodsItem from "../GoodsItem/GoodsItem";

class Goods extends Component {
    render() {
        const {goods, handleBtn} = this.props

        const goodsItems = goods.map(item => {
            return <GoodsItem {...item}
                              handleAddToCart={handleBtn}
                              key={item.vendorCode}
            />
        })

        return (
            <>
                {goodsItems}
            </>
        );
    }
}

export default Goods;