import React, {Component} from 'react';
import Button from "../Button/Button";
import StarIcon from "../StarIcon/StarIcon";
import "./GoodsItem.scss"

class GoodsItem extends Component {
    state = {
        isFavorite: false
    }

    componentDidMount() {
        if (localStorage.getItem(`${this.props.vendorCode}`)) {
            this.setState({
                isFavorite: true
            })
        }
    }

    toggleFavorites() {
        if (!this.state.isFavorite) {
            localStorage.setItem(`${this.props.vendorCode}`, JSON.stringify({
                title: this.props.title,
                color: this.props.color,
                price: this.props.price
            }))

            this.setState({
                isFavorite: !this.state.isFavorite
            })
        } else {
            localStorage.removeItem(`${this.props.vendorCode}`)

            this.setState({
                isFavorite: !this.state.isFavorite
            })
        }
    }

    chooseFavoritesIcon() {
        if (localStorage.getItem(`${this.props.vendorCode}`)) {
            return <StarIcon color="#ffc107"/>
        }

        return <StarIcon color={"gray"}/>
    }

    render() {
        const {title, price, url, vendorCode, color, handleAddToCart} = this.props;

        return (
            <div className="goods-item">
                <img className="goods-item__img" src={url} alt="Brick"/>
                <div className="goods-item__desc">
                    <div className="goods-item__head-container">
                        <h3 className="goods-item__head-text">{title}</h3>
                        <Button text={this.chooseFavoritesIcon()}
                                bgColor={"rgba(0, 0, 0, 0)"}
                                handleBtn={() => this.toggleFavorites()}
                        />
                    </div>
                    <p className="goods-item__text">{`article: ${vendorCode}`}</p>
                    <p className="goods-item__text">{`Color: ${color}`}</p>
                    <div className="goods-item__price-container">
                        <p className="goods-item__text">{`$${price}`}</p>
                        <Button text={"Add to cart"}
                                bgColor={"#606060"}
                                handleBtn={() => handleAddToCart()}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default GoodsItem;