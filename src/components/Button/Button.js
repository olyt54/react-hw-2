import React, {Component} from 'react';
import "./button.scss"

class Button extends Component {
    render() {
        const {text, bgColor, handleBtn} = this.props;

        return (
            <button className="btn" style={{backgroundColor: bgColor}} onClick={() => handleBtn()}>
                {text}
            </button>
        );
    }
}

export default Button;