import React, {Component} from 'react';
import Button from "./components/Button/Button";
import './App.scss';
import Modal from "./components/Modal/Modal";
import axios from "axios";
import Goods from "./components/Goods/Goods";

class App extends Component {
    state = {
        isModalOpen: false,
        goods: []
    }

    getGoods() {
        // const goods = await axios("/goods.json")
        //     .then(res => res.data);
        //
        // this.setState({
        //     goods: goods
        // })

        axios("/goods.json")
            .then(res => this.setState({goods: res.data}))
    }

    componentDidMount() {
        this.getGoods();
    }

    openModal = () => {
        this.setState({
            isModalOpen: true
        })
    }

    closeModal = () => {
        this.setState({
            isModalOpen: false
        })
    }

    addModal() {
        let modal;
        if (this.state.isModalOpen) {
            modal = <Modal
                header={"Adding to cart"}
                text={"You're about to adding this item to cart, are you sure?"}
                closeButton={true}
                handleClose={this.closeModal}
                actions={{
                    firstBtn: <Button text={"Ok"} bgColor={"rgba(0, 0, 0, .5)"} handleBtn={this.closeModal}/>,
                    secondBtn: <Button text={"Cancel"} bgColor={"rgba(0, 0, 0, .5)"} handleBtn={this.closeModal}/>
                }}
            />
        }
        return modal;
    }

    render() {
        const {goods} = this.state

        return (
            <div className="App">
                <div className="goods-container">
                    <Goods goods={goods}
                           handleBtn={this.openModal}
                    />
                </div>
                {this.addModal()}
            </div>
        );
    }
}


export default App;
